﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using Bogus;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace MyRealization.MyGenerator
{
    public class CsvGenerator:IDataGenerator
    {
        private readonly string _fileName;

        public CsvGenerator(string fileName)
        {
            _fileName = fileName;
        }

        public void Generate()
        {
            var clients = RandomCustomerGenerator.Generate(1000000);

           
            using var stream = File.Create(_fileName);
            {                
                CsvSerializer.SerializeToStream(clients, stream);
            }

        }  

       
    }
}
