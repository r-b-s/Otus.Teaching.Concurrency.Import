﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MyRealization.MyRepo
{
    public class MongoRepo : ICustomerRepository
    {

        private readonly IMongoCollection<Customer> _collection;
        public MongoRepo()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");

            var mongoDatabase = mongoClient.GetDatabase("Concurrency");

            _collection = mongoDatabase.GetCollection<Customer>("Customer");
        }

        public void AddCustomer(Customer customer)
        {
            _collection.InsertOne(customer);
        }

        public void Truncate()
        {
            _collection.DeleteMany(x => x.Id > 0);
        }

    
    }
}
