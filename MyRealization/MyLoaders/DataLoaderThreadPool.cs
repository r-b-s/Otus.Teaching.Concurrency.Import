﻿using Bogus.Bson;
using MyRealization.MyParser;
using MyRealization.MyRepo;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataLoaderThreadPool : IDataLoader
    {
        int _thrNum;
        string _filename;
        Barrier barrier;

        public DataLoaderThreadPool(int thrNum, string filename)
        {
            _thrNum = thrNum;
            _filename = filename;
            barrier = new Barrier(thrNum+1);
        }

        public void LoadData()
        {
            int part = (int)(1000000 / _thrNum);
            List<Thread> threads = new List<Thread>();

            var file = File.ReadAllLines(_filename).ToList();


            for (int i = 0; i < _thrNum; i++)
            {
                var from = i * part + 1;
                var cnt = i == _thrNum - 1 ? 1000000 - from + 1 : part;
                var sublist = file.GetRange(from, cnt);

                WaitCallback proc = new WaitCallback(ThreadProc);
                ThreadPool.QueueUserWorkItem(proc,sublist);
            }


            barrier.SignalAndWait();
        }

        void ThreadProc(object obj)
        {
            MongoRepo repo = new MongoRepo();

            CsvParser parser = new CsvParser((List<string>)obj);

            List<Customer> lst = parser.Parse();

            foreach (Customer c in lst)
            {
                repo.AddCustomer(c);
            }
            barrier.SignalAndWait();
        }
    }
}
