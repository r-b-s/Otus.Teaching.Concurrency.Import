using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Diagnostics;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Data;
using MyRealization.MyRepo;
using MyRealization.MyParser;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoaderThread : IDataLoader 
    {
        int _thrNum;
        string _filename;

        public DataLoaderThread(int thrNum,string filename)
        {
            _thrNum = thrNum;
            _filename=filename;                        
        }

        public void LoadData()
        {           

            int part = (int)(1000000 / _thrNum);
            List<Thread> threads = new List<Thread>();

            var file = File.ReadAllLines(_filename).ToList();
            

                for (int i = 0; i < _thrNum; i++)
                {
                var from = i * part + 1;
                var cnt = i == _thrNum - 1 ? 1000000 - from+1 : part ;
                var sublist = file.GetRange(from, cnt);
                Thread t = new Thread(() =>
                    {
                        MongoRepo repo = new MongoRepo();                        

                        CsvParser parser = new CsvParser(sublist);

                        List<Customer> lst = parser.Parse();

                        foreach (Customer c in lst)
                        {
                            repo.AddCustomer(c);
                        }
                    });
                threads.Add(t);
                    t.Start();
                }

                foreach (var t in threads) t.Join();
                
            

        }
    }
}