﻿using MyRealization.MyGenerator;
using MyRealization.MyRepo;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System.Diagnostics;
using System.Numerics;
using System.Reflection.Emit;

namespace MyRealization
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generate csv using command (process or method):");
            Console.WriteLine("gen p <filename>");
            Console.WriteLine("gen m <filename>");
            Console.WriteLine("Load by threads (t) or threadpool(p):");            
            Console.WriteLine("load t <ThreadsNum> <filename>");
            Console.WriteLine("load p <ThreadsNum> <filename>");
            Console.WriteLine("quit");
            var repo = new MongoRepo();
            repo.Truncate();

            while (true)
            {
                var cmd = Console.ReadLine().Split(" ");
                if (cmd[0] == "quit") break;

                try
                {
                    if (cmd[0] == "gen")
                    {
                        if (cmd[1] == "p")
                        {
                            ProcessStartInfo procInfo = new ProcessStartInfo();
                            procInfo.FileName = @"..\..\..\..\MyGeneratorProc\bin\Debug\net6.0\MyGeneratorProc.exe";
                            procInfo.Arguments = cmd[2];
                            Process.Start(procInfo);
                            Console.WriteLine("generator process has been run");
                        }
                        else if (cmd[1] == "m")
                        {
                            var generator = new CsvGenerator(cmd[2]);
                            generator.Generate();
                            Console.WriteLine("generated by method");
                        }
                        else throw new Exception();
                    }



                    if (cmd[0] == "load")
                    {
                        var stopwatch = new Stopwatch();
                        stopwatch.Start();
                        int thrNum = int.Parse(cmd[2]);
                        if (cmd[1] == "t")
                        {
                            var loader=new DataLoaderThread(thrNum, cmd[3]);
                            Console.WriteLine($"{thrNum} threads started...");
                            loader.LoadData();
                        }
                        else if (cmd[1] == "p")
                        {
                            var loader = new DataLoaderThreadPool(thrNum, cmd[3]);
                            Console.WriteLine($"{thrNum} Pool threads started...");
                            loader.LoadData();
                        }
                        else throw new Exception();
                        stopwatch.Stop();
                        Console.WriteLine($"Elapsed: {stopwatch.ElapsedMilliseconds}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unknown command or bad format");
                }
            }

        }
    }
}