﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

namespace MyRealization.MyParser
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        List<string> _data;

        public CsvParser(List<string> data)
        {
            _data=data;
        }

        public List<Customer> Parse()
        {
            ServiceStack.Text.CsvConfig.ItemSeperatorString = ",";

            var res = new List<Customer>();
            foreach (var d in _data)
            {
                //res.Add(  CsvSerializer.DeserializeFromString<Customer>(d));
                var r = d.Split(",");
                res.Add(new Customer
                {
                    Id = int.Parse(r[0]),
                    FullName = r[1],
                    Email = r[2],
                    Phone = r[3]
                    
                });
            }
            return res;
        }
    }
}
