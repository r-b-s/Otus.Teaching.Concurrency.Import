﻿using MyRealization.MyGenerator;
using System.Reflection.Emit;

namespace MyGeneratorProc
{
    internal class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Wrong filename");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("generating...");
            var generator = new CsvGenerator(args[0]);
            generator.Generate();
            Console.WriteLine("done");
            Console.ReadKey();
        }
    }
}